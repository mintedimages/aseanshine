<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Partnermodel extends CI_Model {

	private $lang;

	function __construct()
	{
		parent::__construct();
		$this->lang = $this->session->userdata('lang');
	}

	function getBanner()
	{
		return $this->db
				->select('name, image')
				->where(array('enable_status'=>'show'))
				->where('start_date <= DATE( NOW( ))','',FALSE)
				->where('end_date >= DATE( NOW( ))','',FALSE)
				->order_by('sort_priority', 'asc')
				->get('tbl_partner_slider');
	}

	function getFirstBanner()
	{
		$this->db->where('enable_status', 'show');
		$this->db->where('start_date <= DATE( NOW())', '', FALSE);
		$this->db->where('end_date >= DATE( NOW())', '', FALSE);
		$this->db->order_by('sort_priority', 'asc');
		$this->db->limit(1);
		$result = $this->db->get('tbl_partner_slider');
		return $result->row();
	}

	function getHomeSetting()
	{
		$data = array();
		$data['numrows'] = 0;
		$data['metadescription'] = '';
		$data['metakeyword'] = '';
		$data['eciicasea'] = '';
		$result = $this->db->select('meta_description,meta_keyword,eci_icasea')->where('enable_status','show')->get('tbl_partner');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['numrows'] = $result->num_rows();
			$data['metadescription'] = $row['meta_description'];
			$data['metakeyword'] = $row['meta_keyword'];
			$data['eciicasea'] = $row['eci_icasea'];
		}
		return $data;
	}

	function getOurPartner()
	{
		$lang_id = ($this->lang=='en')? 2 : 1;
		return $this->db
				->join('tbl_our_partner_lang', 'tbl_our_partner_lang.our_partner_id = tbl_our_partner.our_partner_id')
				->where(array(
					'tbl_our_partner.enable_status'=>'show',
					'tbl_our_partner_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_our_partner.sort_priority', 'asc')
				->get('tbl_our_partner');
	}

}

/* End of file partnermodel.php */
/* Location: ./application/models/partnermodel.php */