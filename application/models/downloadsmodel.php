<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Downloadsmodel extends CI_Model {

	private $lang;

	function __construct()
	{
		parent::__construct();
		$this->lang = $this->session->userdata('lang');
	}

	function getBanner()
	{
		return $this->db
				->select('name, image')
				->where(array('enable_status'=>'show'))
				->where('start_date <= DATE( NOW( ))','',FALSE)
				->where('end_date >= DATE( NOW( ))','',FALSE)
				->order_by('sort_priority', 'asc')
				->get('tbl_download_slider');
	}

	function getFirstBanner()
	{
		$this->db->where('enable_status', 'show');
		$this->db->where('start_date <= DATE( NOW())', '', FALSE);
		$this->db->where('end_date >= DATE( NOW())', '', FALSE);
		$this->db->order_by('sort_priority', 'asc');
		$this->db->limit(1);
		$result = $this->db->get('tbl_download_slider');
		return $result->row();
	}

	function getHomeSetting()
	{
		$data = array();
		$data['numrows'] = 0;
		$data['metadescription'] = '';
		$data['metakeyword'] = '';
		$result = $this->db->select('meta_description,meta_keyword')->where('enable_status','show')->get('tbl_download');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['numrows'] = $result->num_rows();
			$data['metadescription'] = $row['meta_description'];
			$data['metakeyword'] = $row['meta_keyword'];
		}
		return $data;
	}

	function displayDownloadsList()
	{
		$html = array();
		$lang_id = ($this->lang=='en')? 2 : 1;
		$cate = $this->db
				->join('tbl_download_file_type_lang', 'tbl_download_file_type_lang.download_file_type_id = tbl_download_file_type.download_file_type_id')
				->where(array(
					'tbl_download_file_type.enable_status'=>'show',
					'tbl_download_file_type_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_download_file_type.sort_priority', 'asc')
				->get('tbl_download_file_type');
		if($cate->num_rows())
		{
			$html[] = '<ul>';
			foreach ($cate->result_array() as $row1) {
				$html[] = '<li><h3>'.$row1['title'].'</h3>';
				$subcate = $this->db
							->join('tbl_download_file_lang', 'tbl_download_file_lang.download_file_id = tbl_download_file.download_file_id')
							->where(array(
								'tbl_download_file.enable_status'=>'show',
								'tbl_download_file_lang.lang_id'=>$lang_id,
								'tbl_download_file.file_type'=>$row1['download_file_type_id']
							))
							->order_by('tbl_download_file.sort_priority', 'asc')
							->get('tbl_download_file');
				if($subcate->num_rows())
				{
					foreach ($subcate->result_array() as $row2) {
						$html[] = '<div class="title">'.$row2['title'].'</div><div class="icon_download"><a href="'.base_url('index.php/downloads/download_file/'.$row2['download_file_lang_id']).'" alt="" target="_blank">'.img('icon-downloads.gif').'</a></div><div class="clearfix"></div>';
					}
				}
				$html[] = '</li>';
			}
			$html[] = '</ul>';
			return join("\n", $html);
		}
	}

	function download_file($id=NULL)
	{
		$this->load->helper('file');
		$this->load->helper('download');
		if($id)
		{
			$result = $this->db->where(array('download_file_lang_id'=>$id))->get('tbl_download_file_lang');
			if($result->num_rows())
			{
				$row = $result->row_array();
				$path = base_url($row['file']);
				$filename = pathinfo($path);
				$data = file_get_contents($path);
				force_download($filename['basename'], $data);
			}
		}
	}

}

/* End of file downloadsmodel.php */
/* Location: ./application/models/downloadsmodel.php */