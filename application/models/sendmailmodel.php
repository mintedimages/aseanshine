<?php

class SendMailModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function smtpmail($email, $subject, $body, $shop_id) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;
        $mail->CharSet = "utf-8";  // ในส่วนนี้ ถ้าระบบเราใช้ tis-620 หรือ windows-874 สามารถแก้ไขเปลี่ยนได้                         
        $mail->Host = $this->ConfigModel->getSMTP($shop_id); //  mail server ของเรา
        //$mail->Port = $this->SiralaiModel->getPort($shop_id);
        $mail->SMTPAuth = true;     //  เลือกการใช้งานส่งเมล์ แบบ SMTP
        $mail->Username = $this->ConfigModel->getSender($shop_id);   //  account e-mail ของเราที่ต้องการจะส่ง
        $mail->Password = $this->ConfigModel->getSenderPassword($shop_id);  //  รหัสผ่าน e-mail ของเราที่ต้องการจะส่ง

        $mail->From = $this->ConfigModel->getSender($shop_id);  //  account e-mail ของเราที่ใช้ในการส่งอีเมล
        $mail->FromName = $this->ConfigModel->getSender($shop_id); //  ชื่อผู้ส่งที่แสดง เมื่อผู้รับได้รับเมล์ของเรา
        $mail->AddAddress($email);            // Email ปลายทางที่เราต้องการส่ง(ไม่ต้องแก้ไข)
        $mail->IsHTML(true);                  // ถ้า E-mail นี้ มีข้อความในการส่งเป็น tag html ต้องแก้ไข เป็น true
        $mail->Subject = $subject;        // หัวข้อที่จะส่ง(ไม่ต้องแก้ไข)
        $mail->Body = $body;                   // ข้อความ ที่จะส่ง(ไม่ต้องแก้ไข)
        $result = $mail->send();
        return $result;
    }

    function smtpSalesMail($subject, $body, $shop_id) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;
        $mail->CharSet = "utf-8";  // ในส่วนนี้ ถ้าระบบเราใช้ tis-620 หรือ windows-874 สามารถแก้ไขเปลี่ยนได้                         
        $mail->Host = $this->ConfigModel->getSMTP($shop_id); //  mail server ของเรา
        //$mail->Port = $this->SiralaiModel->getPort($shop_id);
        $mail->SMTPAuth = true;     //  เลือกการใช้งานส่งเมล์ แบบ SMTP
        $mail->Username = $this->ConfigModel->getSender($shop_id);   //  account e-mail ของเราที่ต้องการจะส่ง
        $mail->Password = $this->ConfigModel->getSenderPassword($shop_id);  //  รหัสผ่าน e-mail ของเราที่ต้องการจะส่ง

        $mail->From = $this->ConfigModel->getSender($shop_id);  //  account e-mail ของเราที่ใช้ในการส่งอีเมล
        $mail->FromName = $this->ConfigModel->getSender($shop_id); //  ชื่อผู้ส่งที่แสดง เมื่อผู้รับได้รับเมล์ของเรา
        $receiverArr = $this->ConfigModel->getReceiver($shop_id);
        if (count($receiverArr) > 0) {
            foreach ($receiverArr as $value) {
                if (trim($value) != '') {
                    $mail->AddAddress(trim($value));            // Email ปลายทางที่เราต้องการส่ง(ไม่ต้องแก้ไข)
                }
            }
        }
        $receiverCCArr = $this->ConfigModel->getReceiverCC($shop_id);
        if (count($receiverCCArr) > 0) {
            foreach ($receiverCCArr as $value) {
                if (trim($value) != '') {
                    $mail->AddCC(trim($value));
                }
            }
        }
        $receiverBCCArr = $this->ConfigModel->getReceiverBCC($shop_id);
        if (count($receiverBCCArr) > 0) {
            foreach ($receiverBCCArr as $value) {
                if (trim($value) != '') {
                    $mail->AddBCC(trim($value));
                }
            }
        }

        $mail->IsHTML(true);                  // ถ้า E-mail นี้ มีข้อความในการส่งเป็น tag html ต้องแก้ไข เป็น true
        $mail->Subject = $subject;        // หัวข้อที่จะส่ง(ไม่ต้องแก้ไข)
        $mail->Body = $body;                   // ข้อความ ที่จะส่ง(ไม่ต้องแก้ไข)
        $result = $mail->send();
        return $result;
    }

}

?>
