<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactusmodel extends CI_Model {

	private $lang;

	function __construct()
	{
		parent::__construct();
		$this->lang = $this->session->userdata('lang');
	}

	function getBanner()
	{
		return $this->db
				->select('name, image')
				->where(array('enable_status'=>'show'))
				->where('start_date <= DATE( NOW( ))','',FALSE)
				->where('end_date >= DATE( NOW( ))','',FALSE)
				->order_by('sort_priority', 'asc')
				->get('tbl_contactus_slider');
	}

	function getFirstBanner()
	{
		$this->db->where('enable_status', 'show');
		$this->db->where('start_date <= DATE( NOW())', '', FALSE);
		$this->db->where('end_date >= DATE( NOW())', '', FALSE);
		$this->db->order_by('sort_priority', 'asc');
		$this->db->limit(1);
		$result = $this->db->get('tbl_contactus_slider');
		return $result->row();
	}

	function getHomeSetting()
	{
		$data = array();
		$data['numrows'] = 0;
		$data['metadescription'] = '';
		$data['metakeyword'] = '';
		$data['address'] = '';
		$data['google_map'] = '';
		$result = $this->db->where('enable_status','show')->get('tbl_contactus');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['numrows'] = $result->num_rows();
			$data['metadescription'] = $row['meta_description'];
			$data['metakeyword'] = $row['meta_keyword'];
			$data['google_map'] = $row['google_map'];
			$data['building_name'] = $row['building_name'];
			$data['address'] = $row['address'];
			$data['email'] = $row['email'];
			$data['tel'] = $row['tel'];
			$data['fax'] = $row['fax'];
			$data['coordinate'] = $row['coordinate'];
		}
		return $data;
	}
    
    function queryPartnerAddress()
    {
        $this->db->where('enable_status','show');
        $query = $this->db->get('tbl_partner_address');
        return $query;
    }

	function sendmail()
	{
		$this->load->helper('file');

		$fname = $this->input->post('Name');
		$surename = $this->input->post('Surname');
		$email = $this->input->post('Email');
		$phone = $this->input->post('Phone');
		$msg = $this->input->post('Message');
		$token = $this->input->post('token');

        $isSpam = false;
        if($this->SpamModel->check_spam($fname))
        {
            $isSpam == true;
        }
        if($this->SpamModel->check_spam($surename))
        {
            $isSpam == true;
        }
        if($this->SpamModel->check_spam($email))
        {
            $isSpam == true;
        }
        if($this->SpamModel->check_spam($phone))
        {
            $isSpam == true;
        }
        if($this->SpamModel->check_spam($msg))
        {
            $isSpam == true;
        }
		if($token == $this->session->userdata('token') && !$isSpam){
			$mail_subject = 'Contact Us - Asean Shine Website (' . $fname.' '. $surename.' '.$email. ')';

			$html = read_file('./assets/frm/frm-contact.html');
			$html = str_replace("[name]", $fname.' '.$surename, $html);
			$html = str_replace("[email]", $email, $html);
			$html = str_replace("[phone]", $phone, $html);
			$html = str_replace("[message]", $msg, $html);

			$Headers = "MIME-Version: 1.0\r\n" ;
			$Headers .= "Content-type: text/html; charset=utf-8\r\n" ;
			$Headers .= "From: ".$email."\r\n" ;
			//mail(MAILTO, $mail_subject, $html, $Headers);
            $this->SendMailModel->smtpSalesMail($mail_subject,$html,1);
			$this->session->set_flashdata('success', 1);
			redirect('contactus', 'refresh');
		}else{
			redirect('contactus', 'refresh');
		}
	}

}

/* End of file contactusmodel.php */
/* Location: ./application/models/contactusmodel.php */