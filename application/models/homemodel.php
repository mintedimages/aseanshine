<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homemodel extends CI_Model {

	private $lang;

	function __construct()
	{
		parent::__construct();
		$this->lang = $this->session->userdata('lang');
	}

	function getBanner()
	{
		return $this->db
				->select('name, sub_name, image')
				->where(array('enable_status'=>'show'))
				->where('start_date <= DATE( NOW( ))','',FALSE)
				->where('end_date >= DATE( NOW( ))','',FALSE)
				->order_by('sort_priority', 'asc')
				->get('tbl_home_slider');
	}

	function getHomeSetting()
	{
		$data = array();
		$data['numrows'] = 0;
		$data['metadescription'] = '';
		$data['metakeyword'] = '';
		$data['vdoclip'] = '';
		$result = $this->db->select('meta_description,vdo_clip,meta_keyword')->where('enable_status','show')->get('tbl_home');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['numrows'] = $result->num_rows();
			$data['metadescription'] = $row['meta_description'];
			$data['metakeyword'] = $row['meta_keyword'];
			$data['vdoclip'] = $row['vdo_clip'];
		}
		return $data;
	}

	function getOneNews()
	{
		$data = array();
		$lang_id = ($this->lang=='en')? 2 : 1;
		$data['news_activity_id'] = array();
		$data['sub_title'] = array();
		$data['numrows'] = 0;
		$result = $this->db
				->join('tbl_news_activity_lang', 'tbl_news_activity_lang.news_activity_id = tbl_news_activity.news_activity_id')
				->where(array(
					'tbl_news_activity.enable_status'=>'show',
					'tbl_news_activity_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_news_activity.sort_priority', 'asc')
				->limit(2)
				->get('tbl_news_activity');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['numrows'] = $result->num_rows();
			foreach ($result->result_array() as $row) {
				$data['news_activity_id'][] = $row['news_activity_id'];
				$data['sub_title'][] = $row['sub_title'];
				$data['thumb'][] = $row['thumb'];
			}
		}
		return $data;
	}

	function getOneInfographic()
	{
		$data = array();
		$lang_id = ($this->lang=='en')? 2 : 1;
		$data['infographic_id'] = array();
		$data['title'] = array();
		$data['numrows'] = 0;
		$result = $this->db
				->join('tbl_infographic_lang', 'tbl_infographic_lang.infographic_id = tbl_infographic.infographic_id')
				->where(array(
					'tbl_infographic.enable_status'=>'show',
					'tbl_infographic_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_infographic.sort_priority', 'asc')
				->limit(2)
				->get('tbl_infographic');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['numrows'] = $result->num_rows();
			foreach ($result->result_array() as $row) {
				$data['infographic_id'][] = $row['infographic_id'];
				$data['title'][] = $row['title'];
				$data['thumb'][] = $row['thumb'];
			}
		}
		return $data;
	}

	function getTextFooter()
	{
		$txt = "";
		$result = $this->db->select("footer")->where("home_id", "1")->get("tbl_home");
		if($result->num_rows()){
			$row = $result->row_array();
			$txt = $row["footer"];
		}
		return '<div class="bottom_txt">'.$txt.'</div>';
	}

	function getYoutube()
	{
		return $this->db->select('title,vdo_clip,thumbnail')->where('enable_status', 'show')->order_by('sort_priority')->get('tbl_youtube');
	}
}

/* End of file homemodel.php */
/* Location: ./application/models/homemodel.php */