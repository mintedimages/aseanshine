<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aboutmodel extends CI_Model {

	private $lang;

	function __construct()
	{
		parent::__construct();
		$this->lang = $this->session->userdata('lang');
	}

	function getBanner()
	{
		return $this->db
				->select('name, sub_name, image')
				->where(array('enable_status'=>'show'))
				->where('start_date <= DATE( NOW( ))','',FALSE)
				->where('end_date >= DATE( NOW( ))','',FALSE)
				->order_by('sort_priority', 'asc')
				->get('tbl_about_slider');
	}

	function getFirstBanner()
	{
		$this->db->where('enable_status', 'show');
		$this->db->where('start_date <= DATE( NOW())', '', FALSE);
		$this->db->where('end_date >= DATE( NOW())', '', FALSE);
		$this->db->order_by('sort_priority', 'asc');
		$this->db->limit(1);
		$result = $this->db->get('tbl_about_slider');
		return $result->row();
	}

	function getHomeSetting()
	{
		$data = array();
		$data['numrows'] = 0;
		$data['metadescription'] = '';
		$data['metakeyword'] = '';
		$result = $this->db->select('meta_description,meta_keyword')->where('enable_status','show')->get('tbl_about');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['numrows'] = $result->num_rows();
			$data['metadescription'] = $row['meta_description'];
			$data['metakeyword'] = $row['meta_keyword'];
		}
		return $data;
	}

	function getAbout()
	{
		$lang_id = ($this->lang=='en')? 2 : 1;
		return $this->db
				->join('tbl_about_asean_shine_lang', 'tbl_about_asean_shine_lang.about_asean_shine_id = tbl_about_asean_shine.about_asean_shine_id')
				->where(array(
					'tbl_about_asean_shine.enable_status'=>'show',
					'tbl_about_asean_shine_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_about_asean_shine.sort_priority', 'asc')
				->get('tbl_about_asean_shine');
	}

	function getInforaphic()
	{
		$lang_id = ($this->lang=='en')? 2 : 1;
		return $this->db
				->join('tbl_infographic_lang', 'tbl_infographic_lang.infographic_id = tbl_infographic.infographic_id')
				->where(array(
					'tbl_infographic.enable_status'=>'show',
					'tbl_infographic_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_infographic.sort_priority', 'asc')
				->get('tbl_infographic');
	}

	function getOneInforaphic($id)
	{
		$data = array();
		$data['title'] = '';
		$data['detail'] = '';
		$data['thumb'] = '';
		$lang_id = ($this->lang=='en')? 2 : 1;
		$result = $this->db
					->join('tbl_infographic', 'tbl_infographic_lang.infographic_id = tbl_infographic.infographic_id')
					->where(array('tbl_infographic_lang.infographic_id'=>$id, 'tbl_infographic_lang.lang_id'=>$lang_id))
					->get('tbl_infographic_lang');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['title'] = $row['title'];
			$data['detail'] = $row['detail'];
			$data['thumb'] = $row['thumb'];
		}
		return $data;
	}

	function getAboutSlide()
	{
		return $this->db
				->select('photo')
				->where(array('enable_status'=>'show'))
				->order_by('sort_priority', 'asc')
				->get('tbl_slider_about');
	}
}

/* End of file aboutmodel.php */
/* Location: ./application/models/aboutmodel.php */