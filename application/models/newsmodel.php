<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsmodel extends CI_Model {

	private $lang;

	function __construct()
	{
		parent::__construct();
		$this->lang = $this->session->userdata('lang');
	}

	function getBanner()
	{
		return $this->db
				->select('name, image')
				->where(array('enable_status'=>'show'))
				->where('start_date <= DATE( NOW( ))','',FALSE)
				->where('end_date >= DATE( NOW( ))','',FALSE)
				->order_by('sort_priority', 'asc')
				->get('tbl_news_slider');
	}

	function getFirstBanner()
	{
		$this->db->where('enable_status', 'show');
		$this->db->where('start_date <= DATE( NOW())', '', FALSE);
		$this->db->where('end_date >= DATE( NOW())', '', FALSE);
		$this->db->order_by('sort_priority', 'asc');
		$this->db->limit(1);
		$result = $this->db->get('tbl_news_slider');
		return $result->row();
	}

	function getHomeSetting()
	{
		$data = array();
		$data['numrows'] = 0;
		$data['metadescription'] = '';
		$data['metakeyword'] = '';
		$result = $this->db->select('meta_description,meta_keyword')->where('enable_status','show')->get('tbl_news');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['numrows'] = $result->num_rows();
			$data['metadescription'] = $row['meta_description'];
			$data['metakeyword'] = $row['meta_keyword'];
		}
		return $data;
	}

	function getNews()
	{
		$lang_id = ($this->lang=='en')? 2 : 1;
		return $this->db
				->join('tbl_news_activity_lang', 'tbl_news_activity_lang.news_activity_id = tbl_news_activity.news_activity_id')
				->where(array(
					'tbl_news_activity.enable_status'=>'show',
					'tbl_news_activity_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_news_activity.sort_priority', 'asc')
				->get('tbl_news_activity');
	}

	function getOneNews($id)
	{
		$data = array();
		$data['title'] = '';
		$data['detail'] = '';
		$data['thumb'] = '';
		$lang_id = ($this->lang=='en')? 2 : 1;
		$result = $this->db
					->join('tbl_news_activity', 'tbl_news_activity_lang.news_activity_id = tbl_news_activity.news_activity_id')
					->where(array('tbl_news_activity_lang.news_activity_id'=>$id, 'tbl_news_activity_lang.lang_id'=>$lang_id))
					->get('tbl_news_activity_lang');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['title'] = $row['title'];
			$data['detail'] = $row['detail'];
			$data['thumb'] = $row['thumb'];
		}
		return $data;
	}

	function getUpcoming()
	{
		$lang_id = ($this->lang=='en')? 2 : 1;
		return $this->db
				->join('tbl_up_coming_events_lang', 'tbl_up_coming_events_lang.up_coming_events_id = tbl_up_coming_events.up_coming_events_id')
				->where(array(
					'tbl_up_coming_events.enable_status'=>'show',
					'tbl_up_coming_events_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_up_coming_events.sort_priority', 'asc')
				->get('tbl_up_coming_events');
	}

	function getOneUpcoming($id)
	{
		$data = array();
		$data['title'] = '';
		$data['detail'] = '';
		$data['thumb'] = '';
		$lang_id = ($this->lang=='en')? 2 : 1;
		$result = $this->db
					->join('tbl_up_coming_events', 'tbl_up_coming_events_lang.up_coming_events_id = tbl_up_coming_events.up_coming_events_id')
					->where(array('tbl_up_coming_events_lang.up_coming_events_id'=>$id, 'tbl_up_coming_events_lang.lang_id'=>$lang_id))
					->get('tbl_up_coming_events_lang');
		if($result->num_rows()){
			$row = $result->row_array();
			$data['title'] = $row['title'];
			$data['detail'] = $row['detail'];
			$data['thumb'] = $row['thumb'];
		}
		return $data;
	}

	function getProjects()
	{
		$lang_id = ($this->lang=='en')? 2 : 1;
		return $this->db
				->join('tbl_project_progress_lang', 'tbl_project_progress_lang.project_progress_id = tbl_project_progress.project_progress_id')
				->where(array(
					'tbl_project_progress.enable_status'=>'show',
					'tbl_project_progress_lang.lang_id'=>$lang_id
				))
				->order_by('tbl_project_progress.sort_priority', 'asc')
				->get('tbl_project_progress');
	}

	function getProjectsSlide()
	{
		return $this->db
				->select('photo')
				->where(array('enable_status'=>'show'))
				->order_by('sort_priority', 'asc')
				->get('tbl_slider_project_progress');
	}
}

/* End of file newsmodel.php */
/* Location: ./application/models/newsmodel.php */