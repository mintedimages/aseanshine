<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_lang extends CI_Controller 
{
	public function index()
	{
		$current_page = urldecode($this->input->get('current_url').'?lang='.$this->session->userdata('lang'));
		redirect($current_page, 'refresh');
	}
}

/* End of file change_lang.php */
/* Location: ./application/controllers/change_lang.php */