<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_progress extends CI_Controller {

	public function index()
	{
		$this->load->model('newsmodel');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->newsmodel->getFirstBanner();
        $dataContent['HomeSetting'] = $this->newsmodel->getHomeSetting();
        $dataContent['Project'] = $this->newsmodel->getProjects();
        $dataContent['ProjectSlide'] = $this->newsmodel->getProjectsSlide();
        $data['content'] = $this->load->view('project-progress', $dataContent, TRUE);
        $data['title'] = 'Project Progress - '.SITENAME;

        $this->load->view('masterpage', $data);
	}

}

/* End of file project_progress.php */
/* Location: ./application/controllers/project_progress.php */