<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	public function index()
	{
		$this->load->model('newsmodel');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->newsmodel->getFirstBanner();
        $dataContent['HomeSetting'] = $this->newsmodel->getHomeSetting();
        $dataContent['News'] = $this->newsmodel->getNews();
        $data['content'] = $this->load->view('news', $dataContent, TRUE);
        $data['title'] = 'News - '.SITENAME;

        $this->load->view('masterpage', $data);
	}

	public function detail($id=NULL)
	{
		if($id)
		{
					$this->load->model('newsmodel');

			        $dataContent = array();
			        $data = array();
			        $dataContent['Banner'] = $this->newsmodel->getFirstBanner();
			        $dataContent['HomeSetting'] = $this->newsmodel->getHomeSetting();
			        $dataContent['News'] = $this->newsmodel->getOneNews($id);
			        $data['content'] = $this->load->view('news-detail', $dataContent, TRUE);
			        $data['title'] = $dataContent['News']['title'].' - '.SITENAME;

			        $this->load->view('masterpage', $data);
		}
		else
		{
			redirect('index.php/news');
		}
	}

}

/* End of file news.php */
/* Location: ./application/controllers/news.php */