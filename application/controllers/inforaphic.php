<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inforaphic extends CI_Controller {

	public function index()
	{
		$this->load->model('aboutmodel');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->aboutmodel->getFirstBanner();
        echo count($dataContent['Banner']);
        $dataContent['HomeSetting'] = $this->aboutmodel->getHomeSetting();
        $dataContent['Inforaphic'] = $this->aboutmodel->getInforaphic();
        $data['content'] = $this->load->view('inforaphic', $dataContent, TRUE);
        $data['title'] = 'Inforaphic - '.SITENAME;

        $this->load->view('masterpage', $data);
	}

    public function detail($id=NULL)
    {
        if($id)
        {
            $this->load->model('aboutmodel');

            $dataContent = array();
            $data = array();
            $dataContent['Banner'] = $this->aboutmodel->getFirstBanner();
            $dataContent['HomeSetting'] = $this->aboutmodel->getHomeSetting();
            $dataContent['Inforaphic'] = $this->aboutmodel->getOneInforaphic($id);
            $data['content'] = $this->load->view('inforaphic-detail', $dataContent, TRUE);
            $data['title'] = $dataContent['Inforaphic']['title'].' - '.SITENAME;

            $this->load->view('masterpage', $data);
        }
        else
        {
            redirect('index.php/inforaphic');
        }
    }

}

/* End of file inforaphic.php */
/* Location: ./application/controllers/inforaphic.php */