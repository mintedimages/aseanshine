<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upcoming extends CI_Controller {

	public function index()
	{
		$this->load->model('newsmodel');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->newsmodel->getFirstBanner();
        $dataContent['HomeSetting'] = $this->newsmodel->getHomeSetting();
        $dataContent['Upcoming'] = $this->newsmodel->getUpcoming();
        $data['content'] = $this->load->view('upcoming', $dataContent, TRUE);
        $data['title'] = 'Upcoming Events - '.SITENAME;

        $this->load->view('masterpage', $data);
	}

	public function detail($id=NULL)
	{
		if($id)
		{
			$this->load->model('newsmodel');

	        $dataContent = array();
	        $data = array();
	        $dataContent['Banner'] = $this->newsmodel->getFirstBanner();
	        $dataContent['HomeSetting'] = $this->newsmodel->getHomeSetting();
	        $dataContent['Upcoming'] = $this->newsmodel->getOneUpcoming($id);
	        $data['content'] = $this->load->view('upcoming-detail', $dataContent, TRUE);
	        $data['title'] = $dataContent['Upcoming']['title'].' - '.SITENAME;

	        $this->load->view('masterpage', $data);
		}
		else
		{
			redirect('index.php/upcoming');
		}
	}

}

/* End of file upcoming.php */
/* Location: ./application/controllers/upcoming.php */