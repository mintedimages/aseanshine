<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $this->load->model('homemodel');
		$this->load->helper('text');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->homemodel->getBanner();
        $dataContent['HomeSetting'] = $this->homemodel->getHomeSetting();
        $dataContent['LinkYoutube'] = $this->homemodel->getYoutube();
        $dataContent['News'] = $this->homemodel->getOneNews();
        $dataContent['Infographic'] = $this->homemodel->getOneInfographic();
        $data['content'] = $this->load->view('home', $dataContent, TRUE);
        $data['title'] = SITENAME;

        $this->load->view('masterpage', $data);
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */