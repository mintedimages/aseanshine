<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Downloads extends CI_Controller {

	public function index()
	{
		$this->load->model('downloadsmodel');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->downloadsmodel->getFirstBanner();
        $dataContent['HomeSetting'] = $this->downloadsmodel->getHomeSetting();
        $dataContent['DownloadsList'] = $this->downloadsmodel->displayDownloadsList();
        $data['content'] = $this->load->view('downloads', $dataContent, TRUE);
        $data['title'] = 'Downloads - '.SITENAME;

        $this->load->view('masterpage', $data);
	}

	public function download_file($id=NULL)
	{
		$this->load->model('downloadsmodel');
		$this->downloadsmodel->download_file($id);
	}

}

/* End of file downloads.php */
/* Location: ./application/controllers/downloads.php */