<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class partner extends CI_Controller {

	public function index()
	{
        $this->load->model('partnermodel');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->partnermodel->getFirstBanner();
        $dataContent['HomeSetting'] = $this->partnermodel->getHomeSetting();
        $dataContent['partner'] = $this->partnermodel->getOurPartner();
        $data['content'] = $this->load->view('partner', $dataContent, TRUE);
        $data['title'] = 'Partners - '.SITENAME;

        $this->load->view('masterpage', $data);
	}

}

/* End of file partner.php */
/* Location: ./application/controllers/partner.php */