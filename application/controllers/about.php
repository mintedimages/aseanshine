<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

    public function index()
    {
        $this->load->model('aboutmodel');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->aboutmodel->getFirstBanner();
        $dataContent['HomeSetting'] = $this->aboutmodel->getHomeSetting();
        $dataContent['About'] = $this->aboutmodel->getAbout();
        $dataContent['AboutSlide'] = $this->aboutmodel->getAboutSlide();
        $data['content'] = $this->load->view('about', $dataContent, TRUE);
        $data['title'] = 'About ASEAN SHINE - '.SITENAME;

        $this->load->view('masterpage', $data);
    }
}

/* End of file about.php */
/* Location: ./application/controllers/about.php */