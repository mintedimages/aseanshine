<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactus extends CI_Controller {

	public function index()
	{
		$this->load->helper('string');
		$this->load->model('contactusmodel');

        $dataContent = array();
        $data = array();
        $dataContent['Banner'] = $this->contactusmodel->getFirstBanner();
        $dataContent['HomeSetting'] = $this->contactusmodel->getHomeSetting();
        $dataContent['token'] = random_string('alnum', 16);
		$this->session->set_userdata('token', $dataContent['token']);
        $data['content'] = $this->load->view('contactus', $dataContent, TRUE);
        $data['title'] = 'Contact Us - '.SITENAME;

        $this->load->view('masterpage', $data);
	}

	public function sendmail()
	{
        $this->load->model('Contactusmodel');
		$this->Contactusmodel->sendmail();
	}

}

/* End of file contactus.php */
/* Location: ./application/controllers/contactus.php */