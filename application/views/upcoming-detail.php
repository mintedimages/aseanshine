<?php if(count($Banner) > 0): ?>
<div class="banner-slider">
    <img class="img-responsive" src="<?php echo (is_file($Banner->image)) ? $Banner->image : base_url($Banner->image); ?>" alt="" />
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="container">
	<div class="navi">Home > News &amp; Activities > <a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>">Upcoming Events</a></div>
    <h1 class="title visible-lg visible-md">News &amp; Activities</h1>
	<div class="row">
    	<div class="col-xs-6 col-md-4">
        	<div class="menu_left">
            	<ul class="visible-lg visible-md">
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>" >News &amp; Activities</a></li>
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/upcoming?lang='.$this->session->userdata('lang'));?>" class="curr">Upcoming Events</a></li>
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/project_progress?lang='.$this->session->userdata('lang'));?>">Project Progress</a></li>
                </ul>
                <div class="menu_left_mobi visible-sm visible-xs">
                    <div class="blockquot"><a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>">News &amp; Activities</a></div>
                    <div class="blockquot"><a href="<?php echo base_url('index.php/upcoming?lang='.$this->session->userdata('lang'));?>" class="curr">Upcoming Events</a></div>
                    <div class="blockquot"><a href="<?php echo base_url('index.php/project_progress?lang='.$this->session->userdata('lang'));?>">Project Progress</a></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
        	<div class="title font_blue" style="margin-top:20px;"><?php echo $Upcoming['title'];?></div>
            <div class="news">
                <div style="margin-top:20px;">
                    <?php echo $Upcoming['detail'];?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Add your site or application content here -->