<?php if(count($Banner) > 0): ?>
<div class="banner-slider">
    <img class="img-responsive" src="<?php echo (is_file($Banner->image)) ? $Banner->image : base_url($Banner->image); ?>" alt="" />
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="container">
	<div class="navi">Home > <a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>">News &amp; Activities</a></div>
    <h1 class="title visible-lg visible-md">News &amp; Activities</h1>
	<div class="row">
    	<div class="col-xs-6 col-md-4">
        	<div class="menu_left">
            	<ul class="visible-lg visible-md">
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>" class="curr">News &amp; Activities</a></li>
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/upcoming?lang='.$this->session->userdata('lang'));?>">Upcoming Events</a></li>
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/project_progress?lang='.$this->session->userdata('lang'));?>">Project Progress</a></li>
                </ul>
                <div class="menu_left_mobi visible-sm visible-xs">
                    <div class="blockquot"><a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>" class="curr">News &amp; Activities</a></div>
                    <div class="blockquot"><a href="<?php echo base_url('index.php/upcoming?lang='.$this->session->userdata('lang'));?>">Upcoming Events</a></div>
                    <div class="blockquot"><a href="<?php echo base_url('index.php/project_progress?lang='.$this->session->userdata('lang'));?>">Project Progress</a></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
        	<div class="blockquot font_blue visible-lg visible-md">News &amp; Activities</div>
            <div class="news">
				<?php if($News->num_rows()): ?>
                <div style="margin-top:20px;">
                	<ul class="media-list">
					<?php foreach($News->result_array() as $rNews): ?>
                        <li class="media">
                            <div class="media">
                            	<?php if($rNews['thumb']): ?>
                            	<a class="pull-left pull-left-cus" href="<?php echo base_url('index.php/news/detail/'.$rNews['news_activity_lang_id'].'?lang='.$this->session->userdata('lang'));?>" title="<?php echo $rNews['title'];?>">
                                    <img class="media-object" src="<?php echo $rNews['thumb'];?>" align="<?php echo $rNews['title'];?>" />
                                </a>
                                <?php endif; ?>
                                <div class="media-body">
                                    <h4 class="media-heading"><?php echo $rNews['title'];?></h4>
                                    <?php echo $rNews['sub_title'];?>
                                </div>
                                <a class="read" href="<?php echo base_url('index.php/news/detail/'.$rNews['news_activity_id'].'?lang='.$this->session->userdata('lang'));?>">Read more >></a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!-- /Add your site or application content here -->