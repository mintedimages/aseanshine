<?php $i=0; if($Banner->num_rows()): ?>
<div id="full-width-slider" class="royalSlider heroSlider rsMinW">
  <?php foreach($Banner->result_array() as $rBanner): ?>
  <div class="rsContent"> <img class="rsImg" src="<?php echo $rBanner['image'];?>" alt="" />
    <?php if($i==0):?>
    <div class="infoBlock infoBlockLeftBlack rsABlock" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
      <h4><?php echo $rBanner['name'];?></h4>
    </div>
    <? endif;?>
  </div>
  <?php $i++; endforeach; ?>
</div>
<?php endif; ?>
<!-- Add your site or application content here -->
<div class="home">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 col-lg-4 tab_news">
        <section>
          <h2><a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>" title="News &amp; Activities">News &amp; Activities</a></h2>
          <div class="detail">
		  	<?php if($News['numrows']): ?>
            <div class="text">
              <p><?php echo $News['sub_title'];?></p>
            </div>
            <a href="<?php echo base_url('index.php/news/detail/'.$News['news_activity_id'].'?lang='.$this->session->userdata('lang'));?>">read >></a>
            <?php endif; ?>
         </div>
        </section>
      </div>
      <div class="col-md-4 col-sm-4 col-lg-4 tab_infographic">
        <section>
          <h2><a href="<?php echo base_url('index.php/inforaphic?lang='.$this->session->userdata('lang'));?>" title="Infographic">Infographic</a></h2>
          <div class="detail">
            <?php if($Infographic['numrows']): ?>
            <div class="text">
              <p><?php echo $Infographic['title'];?></p>
            </div>
            <a href="<?php echo base_url('index.php/inforaphic/detail/'.$Infographic['infographic_id'].'?lang='.$this->session->userdata('lang'));?>">read >></a>
            <?php endif; ?>
          </div>
        </section>
      </div>
      <div class="col-md-4 col-sm-4 col-lg-4 tab_video">
        <section>
          <h2>Vedio</h2>
          <div class="detail">
          <?php
		  if($HomeSetting['numrows']):
		  	if($HomeSetting['vdoclip']):
		  ?>
            <iframe width="100%" height="96%" src="<?php echo $HomeSetting['vdoclip'];?>" frameborder="0" allowfullscreen></iframe>
          <?php
		  	endif;
		  endif;
		  ?>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
<!-- /Add your site or application content here -->