<?php if(count($Banner) > 0): ?>
<div class="banner-slider">
    <img class="img-responsive" src="<?php echo (is_file($Banner->image)) ? $Banner->image : base_url($Banner->image); ?>" alt="" />
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="container">
	<div class="navi">Home > <a href="<?php echo base_url('index.php/partner?lang='.$this->session->userdata('lang'));?>">Partners</a></div>
    <h1 class="title visible-lg visible-md">Partners</h1>
	<div class="row">
    	<div class="col-xs-6 col-md-4">
        	<div class="menu_left">
            	<ul class="visible-lg visible-md">
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/partner?lang='.$this->session->userdata('lang'));?>" class="curr">ECI + ICASEA / Our Partners</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
        	<!--div class="blockquot font_blue">ECI + ICASEA</div-->
            <div class="partner">
				<?php if($HomeSetting['numrows']): ?>
                <?php echo $HomeSetting['eciicasea'];?></p>
                <?php endif; ?>
                <div class="tab_ourpartner">Our Partners</div>
                <div style="margin-top:20px;">
					<?php $i=0;$mod=0; foreach($partner->result_array() as $rpartner): ?>
                    <?php $mod = ($i%2); if($mod==0){echo '<div class="row">';} ?>
                        <div class="col-md-6 col-sm-6 col-xs-8" style="margin-bottom:30px;">
                            <div class="media">
                            	<a class="pull-left" href="#" title="<?php echo $rpartner['title'];?>">
                                    <img class="media-object" src="<?php echo $rpartner['icon'];?>" align="<?php echo $rpartner['title'];?>" />
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading"><?php echo $rpartner['title'];?></h4>
                                    <?php echo $rpartner['detail'];?>
                                </div>
                            </div>
                        </div>
                    <?php if($mod==1){echo '</div>';} ?>
                    <?php $i++;endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Add your site or application content here -->