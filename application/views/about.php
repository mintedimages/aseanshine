<?php if(count($Banner) > 0): ?>
<div class="banner-slider">
    <img class="img-responsive" src="<?php echo (is_file($Banner->image)) ? $Banner->image : base_url($Banner->image); ?>" alt="" />
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="container">
    <div class="navi">Home > <a href="<?php echo base_url('index.php/about?lang='.$this->session->userdata('lang'));?>">About ASEAN SHINE</a></div>
    <h1 class="title visible-lg visible-md">About ASEAN SHINE</h1>
    <div class="row">
        <div class="col-xs-6 col-md-4">
            <div class="menu_left">
                <ul class="visible-lg visible-md">
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/about?lang='.$this->session->userdata('lang'));?>" class="curr">About ASEAN SHINE</a></li>
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/infographic?lang='.$this->session->userdata('lang'));?>">Infographic</a></li>
                </ul>
                <div class="menu_left_mobi visible-sm visible-xs">
                    <div class="blockquot"><a href="<?php echo base_url('index.php/about?lang='.$this->session->userdata('lang'));?>" class="curr">About ASEAN SHINE</a></div>
                    <div class="blockquot"><a href="<?php echo base_url('index.php/infographic?lang='.$this->session->userdata('lang'));?>">Infographic</a></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
            <?php $j=0; if($AboutSlide->num_rows()): ?>
            <div id="projectprogress-slider" class="royalSlider heroSlider rsMinW">
                <?php foreach($AboutSlide->result_array() as $rAboutSlide): ?>
                <div class="rsContent"> <img class="rsImg" src="<?php echo $rAboutSlide['photo'];?>" alt="" /></div>
                <?php $j++; endforeach; ?>
            </div>
            <?php endif; ?>

            <?php if($About->num_rows()): ?>
            <?php foreach($About->result_array() as $rAbout): ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="blockquot"><?php echo $rAbout['title'];?></div>
                    <div class="description"><?php echo htmlspecialchars_decode($rAbout['detail']);?></div>
                </div>
            </div>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($) {
        if($('#projectprogress-slider').length)
        {
            var valAboutControlNavigation = '<?php echo ($AboutSlide->num_rows()>1)? 'bullets' : 'none'; ?>';
            var valAboutSliderDrag = <?php echo ($AboutSlide->num_rows()>1)? 'true' : 'false'; ?>;
            var valAboutSliderTouch = <?php echo ($AboutSlide->num_rows()>1)? 'true' : 'false'; ?>;
            var valAboutKeyboardNavEnabled = <?php echo ($AboutSlide->num_rows()>1)? 'true' : 'false'; ?>;
            var valAboutNavigateByClick = <?php echo ($AboutSlide->num_rows()>1)? 'true' : 'false'; ?>;
            $('#projectprogress-slider').royalSlider({
                arrowsNav: true,
                arrowsNavAutoHide: true,
                loop: true,
                keyboardNavEnabled: valAboutKeyboardNavEnabled,
                controlsInside: false,
                imageScaleMode: 'fill',		
                autoScaleSlider: true,
                autoScaleSliderWidth: 638,
                autoScaleSliderHeight: 251,
                controlNavigation: valAboutControlNavigation,
                thumbsFitInViewport: false,
                sliderDrag: valAboutSliderDrag,
                sliderTouch: valAboutSliderTouch,
                navigateByClick: valAboutNavigateByClick,
                startSlideId: 0,
                autoPlay: false,
                transitionType:'move',
                globalCaption: true,
                deeplinking: {
                    enabled: true,
                    change: false
                }
            });
        }
    });
</script>
<!-- /Add your site or application content here -->