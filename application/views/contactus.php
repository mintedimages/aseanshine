<?php if(count($Banner) > 0): ?>
<div class="banner-slider">
    <img class="img-responsive" src="<?php echo (is_file($Banner->image)) ? $Banner->image : base_url($Banner->image); ?>" alt="" />
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="container">
    <div class="navi">Home > <a href="<?php echo base_url('index.php/contactus?lang='.$this->session->userdata('lang'));?>">Contact Us</a></div>
    <h1 class="title visible-lg visible-md">Contact Us</h1>
    <div class="row">
        <div class="col-xs-6 col-md-4">
            <div class="menu_left">
                <ul class="visible-lg visible-md">
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/contactus?lang='.$this->session->userdata('lang'));?>" class="curr">Contact Us</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
            <div class="contactus">
                <div class="blockquot font_blue">Bangkok Office</div>
                <div class="address_form">
                    <div class="address_box_top">
                        <?php if( $HomeSetting['building_name'] != ""){?>
                        <h1><?php echo $HomeSetting['building_name'];?></h1>
                        <?php }?>
                        <?php if( $HomeSetting['address'] != ""){?>
                        <p><?php echo $HomeSetting['address'];?></p>
                        <?php }?>
                        <?php if( $HomeSetting['email'] != ""){?>
                        <p>Email : <a href="mailto:<?php echo $HomeSetting['email'];?>"><?php echo $HomeSetting['email'];?></a></p>
                        <?php }?>
                        <?php if( $HomeSetting['tel'] != ""){?>
                        <p>Tel: 
                            <a href="tel:<?php echo $HomeSetting['tel'];?>">
                                <?php echo $HomeSetting['tel'];?>
                            </a>
                        </p>
                        <?php }?>
                        <?php if( $HomeSetting['fax'] != ""){?>
                        <p>Fax: <?php echo $HomeSetting['fax'];?></p>
                        <?php }?>
                        <p>&nbsp;</p>
                        <?php if( $HomeSetting['coordinate'] != ""){?>
                        <p>Coordinate: 
                            <a href="<?php echo $HomeSetting['google_map'];?>" target="_blank">
                                <?php echo $HomeSetting['coordinate'];?>
                            </a>
                        </p>
                        <?php }?>
                    </div>
                </div>
                <div class="row">
                    <?php $query = $this->contactusmodel->queryPartnerAddress();
foreach ($query->result() as $row)
{
                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="addres_small">
                            <?php if ($row->name != ""){?><h2><?php echo $row->name;?></h2><?php }?>
                            <?php if ($row->address != ""){?><p><?php echo $row->address;?></p><?php }?>
                            <?php if ($row->email != ""){?>
                            <p>Email : 
                                <a href="mailto:<?php echo $row->email;?>">
                                    <?php echo $row->email;?></a>
                            </p>
                            <?php }?>
                            <?php if ($row->tel != ""){?>
                            <p>Tel: 
                                <a href="tel:<?php echo $row->tel;?>">
                                    <?php echo $row->tel;?>
                                </a>
                            </p>
                            <?php }?>
                            <?php if ($row->fax != ""){?><p>Fax: <?php echo $row->fax;?></p><?php }?>
                            <p>&nbsp;</p>
                            <?php if ($row->coordinate != ""){?>
                            <p>Coordinate: 
                                <a href="<?php echo $row->google_map;?>" target="_blank">
                                    <?php echo $row->coordinate;?>
                                </a>
                            </p>
                            <?php }?>
                        </div>
                    </div>
                    <?php
}
                    ?>
                    <!--
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="addres_small">
<h2>Singapore Office</h2>
<p>Suite 1208, 12th Flr, 591 Sukhumvit Road (Soi 33),</p>
<p>Wattana Bangkok 10110, Thailand</p>
<p>Email : aseanshine@info.com</p>
<p>Tel: (66) 2 662 3465 Fax: (66) 2 261 8615</p>
<p>&nbsp;</p>
<p>Coordinate: 100.56789873, 39.12345</p>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="addres_small">
<h2>Singapore Office</h2>
<p>Suite 1208, 12th Flr, 591 Sukhumvit Road (Soi 33),</p>
<p>Wattana Bangkok 10110, Thailand</p>
<p>Email : aseanshine@info.com</p>
<p>Tel: (66) 2 662 3465 Fax: (66) 2 261 8615</p>
<p>&nbsp;</p>
<p>Coordinate: 100.56789873, 39.12345</p>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="addres_small">
<h2>Singapore Office</h2>
<p>Suite 1208, 12th Flr, 591 Sukhumvit Road (Soi 33),</p>
<p>Wattana Bangkok 10110, Thailand</p>
<p>Email : aseanshine@info.com</p>
<p>Tel: (66) 2 662 3465 Fax: (66) 2 261 8615</p>
<p>&nbsp;</p>
<p>Coordinate: 100.56789873, 39.12345</p>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="addres_small">
<h2>Singapore Office</h2>
<p>Suite 1208, 12th Flr, 591 Sukhumvit Road (Soi 33),</p>
<p>Wattana Bangkok 10110, Thailand</p>
<p>Email : aseanshine@info.com</p>
<p>Tel: (66) 2 662 3465 Fax: (66) 2 261 8615</p>
<p>&nbsp;</p>
<p>Coordinate: 100.56789873, 39.12345</p>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="addres_small">
<h2>Singapore Office</h2>
<p>Suite 1208, 12th Flr, 591 Sukhumvit Road (Soi 33),</p>
<p>Wattana Bangkok 10110, Thailand</p>
<p>Email : aseanshine@info.com</p>
<p>Tel: (66) 2 662 3465 Fax: (66) 2 261 8615</p>
<p>&nbsp;</p>
<p>Coordinate: 100.56789873, 39.12345</p>
</div>
</div>
-->
                </div>
                <div class="blockquot font_blue">Contact ICA Bangkok</div>
                <div class="address_form">
                    <?php if($this->session->flashdata('success')): ?>
                    <div class="alert alert-success" role="alert">Thank you for the information</div>
                    <?php endif; ?>
                    <form name="frmContactus" id="frmContactus" method="post" action="<?php echo site_url('contactus/sendmail');?>">
                        <input type="hidden" name="token" value="<?php echo $token;?>" />
                        <label for="Name">Name</label><input type="text" id="Name" name="Name"  class="validate[required]" data-prompt-position="topLeft" />
                        <div class="row_form"></div>
                        <label for="Surname">Surname</label><input type="text" id="Surname" name="Surname" class="validate[required]" data-prompt-position="topLeft" />
                        <div class="clearfix mb20"></div>
                        <label for="Phone">Phone</label><input type="text" id="Phone" name="Phone" class="validate[required]" />
                        <div class="row_form"></div>
                        <label for="Email">Email</label><input type="text" id="Email" name="Email" class="validate[required,custom[email]]" data-prompt-position="topLeft" />
                        <div class="clearfix mb20"></div>
                        <label for="Message">Message</label><textarea id="Message" name="Message" rows="3" cols="50" class="validate[required]" data-prompt-position="topLeft"></textarea>
                        <div class="clearfix mb20"></div>
                        <div><input type="submit" name="submit" value="Confirm" class="btn_confirm" /></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Add your site or application content here -->
<?php echo js('jquery.validation/js/languages/jquery.validationEngine-en.js');?>
<?php echo js('jquery.validation/js/jquery.validationEngine.js');?>
<script>$("#frmContactus").validationEngine();</script>