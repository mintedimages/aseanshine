<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="<?php echo $HomeSetting['metadescription'];?>">
    <meta name="keywords" content="<?php echo $HomeSetting['metakeyword'];?>">

    <title><?php echo $title;?></title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <?php /*?><link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico');?>" /><?php */?>

    <?php echo css('normalize.css');?>
    <?php echo css('html5boilerplate-main.css');?>
    <!-- Bootstrap -->
    <?php echo css('bootstrap.min.css');?>
    <!-- End Bootstrap -->

    <!-- jquery validation -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/jquery.validation/css/validationEngine.jquery.css');?>" media="all">
    <!-- End jquery validation -->

    <!-- bxSlider CSS file -->
    <link href="<?php echo base_url('assets/js/jquery.bxslider/jquery.bxslider.css');?>" rel="stylesheet" />

    <?php echo css('responsivemobilemenu.css');?><?php echo css('theme.css');?>
    <!--[if IE 7]><?php echo css('ie7-only.css');?><![endif]-->
    <!--[if IE 8]><?php echo css('ie8-only.css');?><![endif]-->
    <!--[if IE 9]><?php echo css('ie9-only.css');?><![endif]-->
    <!--[if lt IE 9]>
        <?php echo js('html5.js');?>
        <?php echo js('css3-mediaqueries.js');?>
        <?php echo js('respond.min.js');?>
    <![endif]-->
    <?php echo js('modernizr-2.6.2.min.js');?>
    <?php echo js('jquery-1.10.2.min.js');?>
    <!-- bxSlider Javascript file -->
    <script src="<?php echo base_url('assets/js/jquery.bxslider/jquery.bxslider.min.js');?>"></script>
    <?php echo $this->ConfigModel->getGoogleAnalytic();?>
</head>
<body>
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Header start -->
    <header>
        <div class="container">
            <h1><a href="<?php echo base_url();?>" title="Welcome to Asean Shine"><?php echo img('logo.png');?></a></h1>
        </div>
        <?php
            $segment_menu = $this->uri->segment(1);
            $home_menu = '';
            $about_menu = '';
            $partner_menu = '';
            $news_menu = '';
            $downloads_menu = '';
            $contactus_menu = '';
            $data_menu = '';
            switch($segment_menu){
                case '': $home_menu = 'current'; $data_menu = 'home';
                case 'home': $home_menu = 'current'; $data_menu = 'home';
                break;
                case 'about': $about_menu = 'current'; $data_menu = 'about';
                case 'infographic': $about_menu = 'current'; $data_menu = 'about';
                break;
                case 'partner': $partner_menu = 'current'; $data_menu = 'partners';
                break;
                case 'news': $news_menu = 'current'; $data_menu = 'news';
                case 'upcoming': $news_menu = 'current'; $data_menu = 'news';
                case 'project_progress': $news_menu = 'current'; $data_menu = 'news';
                break;
                case 'downloads': $downloads_menu = 'current'; $data_menu = 'downloads';
                break;
                case 'contactus': $contactus_menu = 'current'; $data_menu = 'contactus';
            }
        ?>
        <div class="menu">
            <div class="container desktop">
                <div class="items <?php echo $home_menu;?>"><a href="<?php echo base_url();?>" title="Home">Home</a></div>
                <div class="items <?php echo $about_menu;?>"><a href="<?php echo base_url('index.php/about?lang='.$this->session->userdata('lang'));?>" title="About Asean Shine">About Asean Shine</a></div>
                <div class="items <?php echo $partner_menu;?>"><a href="<?php echo base_url('index.php/partner?lang='.$this->session->userdata('lang'));?>" title="Partners">Partners</a></div>
                <div class="items <?php echo $news_menu;?>"><a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>" title="News &amp; Activities">News &amp; Activities</a></div>
                <div class="items <?php echo $downloads_menu;?>"><a href="<?php echo base_url('index.php/downloads?lang='.$this->session->userdata('lang'));?>" title="Downloads">Downloads</a></div>
                <div class="items <?php echo $contactus_menu;?> <?php /*?>last<?php */?>"><a href="<?php echo base_url('index.php/contactus?lang='.$this->session->userdata('lang'));?>" title="Contact Us">Contact Us</a></div>
                <?php /*?>
                <div class="items last">
                    <?php if($this->session->userdata('lang')=='th'):?>
                        <a href="javascript:void(0);" title="TH" class="current_link">TH</a>
                    <?php else: ?>
                        <a href="<?php echo base_url('index.php/change_lang?lang=th&current_url=').urlencode(uri_string());?>" title="TH">TH</a>
                    <?php endif; ?>
                        &nbsp;|&nbsp;
                    <?php if($this->session->userdata('lang')=='en'):?>
                        <a href="javascript:void(0);" title="EN" class="current_link">EN</a>
                    <?php else: ?>
                        <a href="<?php echo base_url('index.php/change_lang?lang=en&current_url=').urlencode(uri_string());?>" title="EN">EN</a>
                    <?php endif; ?>
                </div>
                <?php */?>
            </div>
            <div class='rmm' data-menu-style = "custom" data-menu-title="<?php echo $data_menu;?>">
                <ul>
                    <li><a href='<?php echo base_url();?>#home'>Home</a></li>
                    <li><a href='<?php echo base_url('index.php/about?lang='.$this->session->userdata('lang'));?>#about'>About Asean Shine</a></li>
                    <li><a href='<?php echo base_url('index.php/partner?lang='.$this->session->userdata('lang'));?>#partner'>Partners</a></li>
                    <li><a href='<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>#news'>News & Activities</a></li>
                    <li><a href='<?php echo base_url('index.php/downloads?lang='.$this->session->userdata('lang'));?>#downloads'>Dowloads</a></li>
                    <li><a href='<?php echo base_url('index.php/contactus?lang='.$this->session->userdata('lang'));?>#contactus'>Contact Us</a></li>
                </ul>
            </div>
            <?php /*?>
            <div style="margin-top:48px;"></div>
            <div class='rmm menu_lang' data-menu-style = "lang" data-menu-title="<?php echo $this->session->userdata('lang');?>">
                <ul>
                    <li><a href='<?php echo base_url('index.php/change_lang?lang=en&current_url=').urlencode(uri_string());?>#en'>En</a></li>
                    <li><a href='<?php echo base_url('index.php/change_lang?lang=th&current_url=').urlencode(uri_string());?>#th'>Th</a></li>
                </ul>
            </div>
            <?php */?>
        </div>
        <div class="bg"></div>
    </header>
    <!-- Header end -->

    <!-- Main start -->
    <?php echo $content;?>
    <!-- /Main end -->

    <!-- Footer start -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="banner1 col-sm-6"> <?php echo img('banner_footer1.png', array('class'=>'img-responsive'));?> <div class="txt_link"><a href="http://ec.europa.eu/europeaid/index_en.htm" target="_blank">http://ec.europa.eu/europeaid</a></div></div>
                <div class="banner2 col-sm-6"> <?php echo img('banner_footer2.png', array('class'=>'img-responsive pull-right'));?> </div>
            </div>
            <div class="row menu">
                <div class="left col-sm-6"><a href="<?php echo base_url();?>" title="Home">HOME</a> I <a href="<?php echo base_url('index.php/about?lang='.$this->session->userdata('lang'));?>" title="About Asean Shine">ABOUT ASEAN SHINE</a> I <a href="<?php echo base_url('index.php/partner?lang='.$this->session->userdata('lang'));?>" title="Partners">PARTNERS</a> I <a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>" title="News &amp; Activities">NEWS &amp; ACTIVITES</a> I <a href="<?php echo base_url('index.php/downloads?lang='.$this->session->userdata('lang'));?>" title="Downloads">DOWNLOADS</a> I <a href="<?php echo base_url('index.php/contactus?lang='.$this->session->userdata('lang'));?>" title="Contact Us">CONTACT US</a></div>
                <div class="right col-sm-6">&copy; Copyright 2014 Asean Shine, Ltd. All Rights Reserved.</div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $this->homemodel->getTextFooter();?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="bottom_social">
                        <span class='st_facebook_large' displayText='Facebook'></span>
                        <span class='st_twitter_large' displayText='Tweet'></span>
                        <span class='st_googleplus_large' displayText='Google +'></span>
                        <span class='st_linkedin_large' displayText='LinkedIn'></span>
                        <span class='st_pinterest_large' displayText='Pinterest'></span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- /Footer end -->
    
    <?php echo js('bootstrap.min.js');?>
    <?php echo js('responsivemobilemenu.js');?>
    <?php echo js('jquery.royalslider.min.js');?><?php echo js('jquery.dotdotdot/src/js/jquery.dotdotdot.min.js');?>
    <script>
        jQuery(document).ready(function($) {
            var valControlNavigation = '<?php echo ($Banner->num_rows()>1)? 'bullets' : 'none'; ?>';
            var valSliderDrag = <?php echo ($Banner->num_rows()>1)? 'true' : 'false'; ?>;
            var valSliderTouch = <?php echo ($Banner->num_rows()>1)? 'true' : 'false'; ?>;
            var valKeyboardNavEnabled = <?php echo ($Banner->num_rows()>1)? 'true' : 'false'; ?>;
            var valNavigateByClick = <?php echo ($Banner->num_rows()>1)? 'true' : 'false'; ?>;
            $('#full-width-slider').royalSlider({
                arrowsNav: true,
                arrowsNavAutoHide: true,
                loop: true,
                keyboardNavEnabled: valKeyboardNavEnabled,
                controlsInside: false,
                imageScaleMode: 'fill',
                autoScaleSlider: true,
                autoScaleSliderWidth: 1498,
                autoScaleSliderHeight: 590,
                controlNavigation: valControlNavigation,
                thumbsFitInViewport: false,
                sliderDrag: valSliderDrag,
                sliderTouch: valSliderTouch,
                navigateByClick: valNavigateByClick,
                startSlideId: 0,
                autoPlay: {
                    // autoplay options go gere
                    enabled: true,
                    stopAtAction: false,
                    pauseOnHover: false,
                    delay:5000
                },
                transitionType:'move',
                globalCaption: true,
                deeplinking: {
                    enabled: true,
                    change: false
                }
            });

            if($('html').hasClass('ie7')){
                $('.col-md-4').css('width','28.33333333%');
                $('.col-md-8').css('width','61.33333333%');
                $('.col-xs-8').css('width','43.33333333%');
                $('.col-md-6').css('width','44.33333333%');
            }

            $('.bxslider').bxSlider({
                slideWidth: 100, 
                maxSlides: 8,
                minSlides: 3,
                slideMargin: 0,
                pager:false
            });

            if($("ul.youtube_list").length){
                $("ul.youtube_list a").on('click', function(e){
                    e.preventDefault();
                    $("#iframvdo").attr("src", $(this).attr("href"));
                    return false;
                });
            }

            $("ul.youtube_list a:eq(0)").trigger('click');

            if($(".detail1").length){
                $('.text').dotdotdot();
            }
        });
    </script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "b374fb74-e2e1-41bf-99a2-7c3bec0f7e0c", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    <?php echo $this->ConfigModel->getGoogleRemarketing();?>
</body>
</html>