<?php if(count($Banner) > 0): ?>
<div class="banner-slider">
    <img class="img-responsive" src="<?php echo (is_file($Banner->image)) ? $Banner->image : base_url($Banner->image); ?>" alt="" />
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="container">
	<div class="navi">Home > About ASEAN SHINE > <a href="<?php echo base_url('index.php/infographic?lang='.$this->session->userdata('lang'));?>">Infographic</a></div>
    <h1 class="title visible-lg visible-md">About ASEAN SHINE</h1>
	<div class="row">
    	<div class="col-xs-6 col-md-4">
        	<div class="menu_left">
            	<ul class="visible-lg visible-md">
                	<li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/about?lang='.$this->session->userdata('lang'));?>">About ASEAN SHINE</a></li>
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/infographic?lang='.$this->session->userdata('lang'));?>" class="curr">Infographic</a></li>
                </ul>
                <div class="menu_left_mobi visible-sm visible-xs">
                    <div class="blockquot"><a href="<?php echo base_url('index.php/about?lang='.$this->session->userdata('lang'));?>">About ASEAN SHINE</a></div>
                    <div class="blockquot"><a href="<?php echo base_url('index.php/infographic?lang='.$this->session->userdata('lang'));?>" class="curr">Infographic</a></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
        <div class="blockquot font_blue visible-lg visible-md">All Infographic</div>
        <?php if($Inforaphic->num_rows()): ?>
        <div style="margin-top:20px;">
        	<?php $i=0;$mod=0; foreach($Inforaphic->result_array() as $rInforaphic): ?>
            <?php $mod = ($i%2); if($mod==0){echo '<div class="row">';} ?>
            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                	<div class="inforaphic">
                    	<div class="left"><a href="<?php echo base_url('index.php/infographic/detail/'.$rInforaphic['infographic_id'].'?lang='.$this->session->userdata('lang'));?>"><img src="<?php echo $rInforaphic['thumb'];?>" /></a></div>
                        <div class="right">
							<div class="title"><?php echo $rInforaphic['title'];?></div>
                            <a class="read" href="<?php echo base_url('index.php/infographic/detail/'.$rInforaphic['infographic_id'].'?lang='.$this->session->userdata('lang'));?>">Read more >></a>
                        </div>
                    </div>
                </div>
            <?php if($mod==1){echo '</div>';} ?>
            <?php $i++;endforeach; ?>
        </div>
        <?php endif; ?>
        </div>
    </div>
</div>
<!-- /Add your site or application content here -->