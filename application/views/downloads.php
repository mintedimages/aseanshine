<?php if(count($Banner) > 0): ?>
<div class="banner-slider">
    <img class="img-responsive" src="<?php echo (is_file($Banner->image)) ? $Banner->image : base_url($Banner->image); ?>" alt="" />
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="container">
	<div class="navi">Home > <a href="<?php echo base_url('index.php/downloads?lang='.$this->session->userdata('lang'));?>">Downloads</a></div>
    <h1 class="title visible-lg visible-md">Downloads</h1>
	<div class="row">
    	<div class="col-xs-6 col-md-4">
        	<div class="menu_left">
            	<ul class="visible-lg visible-md">
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/downloads?lang='.$this->session->userdata('lang'));?>" class="curr">Downloads</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
        	<div class="blockquot font_blue">Downloads</div>
            <div class="downloads"><?php echo $DownloadsList;?> </div>
        </div>
    </div>
</div>
<!-- /Add your site or application content here -->