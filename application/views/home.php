<?php $i=0; if($Banner->num_rows()): ?>
<div id="full-width-slider" class="royalSlider heroSlider rsMinW">
    <?php foreach($Banner->result_array() as $rBanner): ?>
    <div class="rsContent">
        <img class="rsImg" src="<?php echo (is_file($rBanner['image'])) ? $rBanner['image']:base_url($rBanner['image']); ?>" alt="" />
        <?php if($rBanner['name'] !="" || $rBanner['sub_name'] != ""): ?>
        <div class="infoBlock infoBlockLeftBlack rsABlock" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
            <?php if ($rBanner['name'] != ""): ?>
                <h4><?php echo $rBanner['name']; ?></h4>
            <?php endif ?>
            <?php if ($rBanner['sub_name'] != ""): ?>
                <div class="sub-name <?php echo ($rBanner['name'] == "") ? 'no-head':''; ?>"><?php echo $rBanner['sub_name']; ?></div>
            <?php endif ?>
        </div>
        <?php endif; ?>
    </div>
    <?php $i++; endforeach; ?>
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="home">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="">
                    <div class="tab_news">
                        <section>
                            <h2><a class="tab" href="<?php echo base_url("index.php/news");?>" title="News &amp; Activities">News &amp; Activities</a></h2>
                        </section>
                    </div>
                </div>
                <div id="boxnews" class="">
                    <?php if($News['numrows']): ?>
                    <div class="detail1">
                        <div class="thumb"><img src="<?php echo $News['thumb'][0];?>" alt="" class="img-responsive"/></div>
                        <div class="text">					
                            <p><?php echo $News['sub_title'][0];?></p>
                        </div>
                        <a href="<?php echo base_url('index.php/news/detail/'.$News['news_activity_id'][0].'?lang='.$this->session->userdata('lang'));?>">Read more >></a>
                    </div>
                    <?php else: ?>
                    <div class="detail1">
                        <div class="text"><p>&nbsp;</p></div>
                    </div>
                    <?php endif;?>

                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tab_video" style="padding-left:0;">
                <div class="">
                    <div class="tab_infographic" style="padding-left:0;">
                        <section>
                            <h2><a class="tab" href="<?php echo base_url("index.php/infographic");?>" title="Infographic">Infographic</a></h2>
                        </section>
                    </div>
                </div>
                <div id="boxnews" class="">     
                    <?php if($Infographic['numrows']): ?>
                    <div class="detail2">
                        <div class="thumb"><img src="<?php echo $Infographic['thumb'][0];?>" alt="" class="img-responsive"/></div>
                        <div class="text">					
                            <p><?php echo $Infographic['title'][0];?></p>
                        </div>
                        <a href="<?php echo base_url('index.php/inforaphic/detail/'.$Infographic['infographic_id'][0].'?lang='.$this->session->userdata('lang'));?>">Read more >></a>
                    </div>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tab_video" style="padding-left:0;">
                <section>
                    <h2>Video</h2>
                    <div class="detail">
                        <div class="embed-responsive embed-responsive-16by9">
                            <?php
if($HomeSetting['numrows']):
if($HomeSetting['vdoclip']):
                            ?>
                            <iframe id="iframvdo" class="embed-responsive-item" src="<?php echo $HomeSetting['vdoclip'];?>" frameborder="0" allowfullscreen></iframe>
                            <?php
endif;
endif;
                            ?>
                        </div>
                        <?php
if($LinkYoutube->num_rows()){
    echo '<ul class="youtube_list bxslider">';
    foreach($LinkYoutube->result_array() as $rYoutube){
        echo '<li><a href="'.$rYoutube['vdo_clip'].'"><img src="'.$rYoutube['thumbnail'].'"/></a></li>';
    }
    echo '</ul>';
}
                        ?>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- /Add your site or application content here -->