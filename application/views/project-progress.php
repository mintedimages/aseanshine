<?php if(count($Banner) > 0): ?>
<div class="banner-slider">
    <img class="img-responsive" src="<?php echo (is_file($Banner->image)) ? $Banner->image : base_url($Banner->image); ?>" alt="" />
</div>
<?php endif; ?>

<!-- Add your site or application content here -->
<div class="container">
	<div class="navi">Home > News &amp; Activities > <a href="<?php echo base_url('index.php/project_progress?lang='.$this->session->userdata('lang'));?>">Project Progress</a></div>
    <h1 class="title visible-lg visible-md">Project Progress</h1>
	<div class="row">
    	<div class="col-xs-6 col-md-4">
        	<div class="menu_left">
            	<ul class="visible-lg visible-md">
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>">News &amp; Activities</a></li>
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/upcoming?lang='.$this->session->userdata('lang'));?>">Upcoming Events</a></li>
                    <li><span class="glyphicon glyphicon-play btn-cu"></span> <a href="<?php echo base_url('index.php/project_progress?lang='.$this->session->userdata('lang'));?>" class="curr">Project Progress</a></li>
                </ul>
                <div class="menu_left_mobi visible-sm visible-xs">
                    <div class="blockquot"><a href="<?php echo base_url('index.php/news?lang='.$this->session->userdata('lang'));?>">News &amp; Activities</a></div>
                    <div class="blockquot"><a href="<?php echo base_url('index.php/upcoming?lang='.$this->session->userdata('lang'));?>">Upcoming Events</a></div>
                    <div class="blockquot"><a href="<?php echo base_url('index.php/project_progress?lang='.$this->session->userdata('lang'));?>" class="curr">Project Progress</a></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
        	<?php $j=0; if($ProjectSlide->num_rows()): ?>
        	<div id="projectprogress-slider" class="royalSlider heroSlider rsMinW">
				<?php foreach($ProjectSlide->result_array() as $rProjectSlide): ?>
                <div class="rsContent"> <img class="rsImg" src="<?php echo $rProjectSlide['photo'];?>" alt="" /></div>
                <?php $j++; endforeach; ?>
            </div>
            <?php endif; ?>
        	<div class="blockquot font_blue visible-lg visible-md">Project Progress</div>
            <div class="news">
            <?php if($Project->num_rows()):?>
            	<?php foreach($Project->result_array() as $rProject):?>
            	<ul class="media-list">
                	<li class="media">
                    	<span class="date">Latest Update  I  <?php echo date("d.m.y", strtotime($rProject['update_date']));?></span>
                    	<a class="pull-left project_title" href="#"><div class="media-object"><?php echo $rProject['title'];?></div></a>
                		<div class="media-body">
                        	<div class="project_body"><?php echo $rProject['detail'];?></div>
                            <a class="pull-right" href="#"><div class="media-object project_percent"><?php echo $rProject['percentage'];?></div></a>
                        </div>
                    </li>
                </ul>
                <?php endforeach;?>
            <?php endif;?>
            </div>
        </div>
    </div>
</div>
<script>
jQuery(document).ready(function($) {
	if($('#projectprogress-slider').length)
	{
		var valProjectControlNavigation = '<?php echo ($ProjectSlide->num_rows()>1)? 'bullets' : 'none'; ?>';
		var valProjectSliderDrag = <?php echo ($ProjectSlide->num_rows()>1)? 'true' : 'false'; ?>;
		var valProjectSliderTouch = <?php echo ($ProjectSlide->num_rows()>1)? 'true' : 'false'; ?>;
		var valProjectKeyboardNavEnabled = <?php echo ($ProjectSlide->num_rows()>1)? 'true' : 'false'; ?>;
		var valProjectNavigateByClick = <?php echo ($ProjectSlide->num_rows()>1)? 'true' : 'false'; ?>;
		$('#projectprogress-slider').royalSlider({
			arrowsNav: true,
			arrowsNavAutoHide: true,
			loop: true,
			keyboardNavEnabled: valProjectKeyboardNavEnabled,
			controlsInside: false,
			imageScaleMode: 'fill',		
			autoScaleSlider: true,
			autoScaleSliderWidth: 638,
			autoScaleSliderHeight: 251,
			controlNavigation: valProjectControlNavigation,
			thumbsFitInViewport: false,
			sliderDrag: valProjectSliderDrag,
			sliderTouch: valProjectSliderTouch,
			navigateByClick: valProjectNavigateByClick,
			startSlideId: 0,
			autoPlay: false,
			transitionType:'move',
			globalCaption: true,
			deeplinking: {
				enabled: true,
				change: false
			}
		});
	}
});
</script>
<!-- /Add your site or application content here -->